import {ChangeDetectorRef, Component, NgZone, OnInit} from '@angular/core';

import {Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {WalletService} from './services/wallet.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;
  public appPages = [
    {
      title: 'Адреса',
      url: '/address-list',
      icon: 'wallet'
    },
    {
      title: 'Транзакции',
      url: '/transaction-list',
      icon: 'list'
    },


  ];

  constructor(
      private platform: Platform,
      private splashScreen: SplashScreen,
      private statusBar: StatusBar,
      private walletService: WalletService,
      private zone: NgZone,
      private changeDetector: ChangeDetectorRef
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(async () => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      const restored = await this.walletService.restore();
      console.log('restored', restored);
      if (!restored) {
        const mnemonic = await this.walletService.generateNewWallet();
        this.changeDetector.detectChanges();

      }
      const addresses = this.walletService.getAddresses();
      addresses.forEach(value => {
        console.log(value);
      });


    });
  }

  ngOnInit() {
    const path = window.location.pathname.split('folder/')[1];
    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    }
  }
}
