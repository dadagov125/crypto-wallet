import {Injectable} from '@angular/core';
import {Plugins} from '@capacitor/core';
import * as bip39 from 'bip39';
import * as bip32 from 'bip32';
import * as bitcoin from 'bitcoinjs-lib';
import {HttpClient} from '@angular/common/http';
import {Transaction} from '../models/Transaction';
import {environment} from 'src/environments/environment';

const {Storage} = Plugins;
const {restApiUrl} = environment;

@Injectable({
  providedIn: 'root'
})
export class WalletService {

  wallet: bip32.BIP32Interface;
  addressCount = 10;

  constructor(private http: HttpClient) {

  }

  get initial() {
    return this.wallet != null;
  }

  getAddresses(): string[] {
    if (!this.initial) {
      return [];
    }

    const array = Array.from(new Array(this.addressCount), (x, i) => i + 1);
    return array.map(((value, index) => this.getAddress(this.wallet.derivePath(`m/0\'/0/${index}`))));
  }

  getAddress(node: bip32.BIP32Interface, network: bitcoin.networks.Network = bitcoin.networks.testnet): string {
    // tslint:disable-next-line:no-non-null-assertion
    const payment = bitcoin.payments.p2pkh({pubkey: node.publicKey, network});
    return payment.address;
  }

  async incrementAddressCount() {
    this.addressCount = ++this.addressCount;
    await Storage.set({key: 'wallet_address_count', value: this.addressCount.toString()});
  }

  async generateNewWallet() {
    const mnemonic = bip39.generateMnemonic();
    const seed = bip39.mnemonicToSeedSync(mnemonic);
    this.wallet = bip32.fromSeed(seed);
    await Storage.set({key: 'wallet', value: this.wallet.toBase58()});
    await Storage.set({key: 'wallet_address_count', value: '10'});

    return mnemonic;
  }

  async restore() {
    try {
      const storedWalletBase58 = await Storage.get({key: 'wallet'});
      const walletBase58 = storedWalletBase58?.value;
      if (walletBase58) {
        this.wallet = bip32.fromBase58(walletBase58);
        const storedAddressCount = await Storage.get({key: 'wallet_address_count'});
        const addressCount = storedAddressCount?.value;
        this.addressCount = addressCount ? Number(addressCount) : 10;
      }
    } catch (e) {

    }

    return !!this.wallet;

    // console.log('address', this.getAddress(this.wallet.derivePath('m/0\'/0/1')));

  }


  getTransactions(address) {
    return this.http.get<Transaction[]>(`${restApiUrl}/transactions/${address}`);
  }

  getAllTransactions(addresses: string[]) {
    return this.http.post<Transaction[]>(`${restApiUrl}/transactions`, addresses);
  }

  getBalance(address) {
    return this.http.get<{ address: string, balance: number }>(`${restApiUrl}/balance/${address}`);
  }

}
