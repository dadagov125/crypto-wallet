import {Component, NgZone, OnInit} from '@angular/core';
import {WalletService} from '../../services/wallet.service';
import {Transaction} from '../../models/Transaction';

@Component({
  selector: 'app-transaction-list',
  templateUrl: './transaction-list.page.html',
  styleUrls: ['./transaction-list.page.scss'],
})
export class TransactionListPage implements OnInit {

  constructor(private walletService: WalletService, private zone: NgZone) {
  }

  txs: Transaction[];

  addresses: string[];

  ngOnInit() {
    this.addresses = this.walletService.getAddresses();
    this.walletService.getAllTransactions(this.addresses).subscribe(value => {
      this.txs = value;
    });
  }


  getMyAddress(tx: Transaction) {

    if (this.addresses.includes(tx.addressOut)) {
      return tx.addressOut;
    }
    if (this.addresses.includes(tx.addressIn)) {
      return tx.addressIn;
    }

  }

  getOtherAddress(tx: Transaction) {
    if (!this.addresses.includes(tx.addressOut)) {
      return tx.addressOut;
    }
    if (!this.addresses.includes(tx.addressIn)) {
      return tx.addressIn;
    }
  }

  isIncoming(tx: Transaction) {
    return this.addresses.includes(tx.addressIn);

  }

}
