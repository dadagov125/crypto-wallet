import {Component, NgZone, OnInit} from '@angular/core';
import {WalletService} from '../../services/wallet.service';
import {forkJoin} from 'rxjs';

@Component({
  selector: 'app-address-list',
  templateUrl: './address-list.page.html',
  styleUrls: ['./address-list.page.scss'],
})
export class AddressListPage implements OnInit {

  addresses: Array<{ address: string, balance: number }>;

  constructor(private walletService: WalletService, private zone: NgZone) {
  }

  ngOnInit() {
    forkJoin(this.walletService.getAddresses().map(address => this.walletService.getBalance(address))).subscribe(value => {
      this.addresses = value;
    });

  }

  addAddress() {
    this.zone.run(() => {
      this.walletService.incrementAddressCount();
      forkJoin(this.walletService.getAddresses().map(address => this.walletService.getBalance(address))).subscribe(value => {
        this.addresses = value;
      });
    });


  }


}
