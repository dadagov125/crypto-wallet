import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddressListPage } from './address-list.page';

const routes: Routes = [
  {
    path: '',
    component: AddressListPage
  },
  {
    path: 'transactions/:address',
    loadChildren: () => import('./transactions/transactions.module').then( m => m.TransactionsPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddressListPageRoutingModule {}
