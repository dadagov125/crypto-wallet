import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {WalletService} from '../../../services/wallet.service';
import {Transaction} from '../../../models/Transaction';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.page.html',
  styleUrls: ['./transactions.page.scss'],
})
export class TransactionsPage implements OnInit {

  address: string;

  txs: Observable<Transaction[]>;

  constructor(private activatedRoute: ActivatedRoute, private walletService: WalletService) {
  }

  ngOnInit() {
    this.address = this.activatedRoute.snapshot.paramMap.get('address');
    this.txs = this.walletService.getTransactions(this.address);

  }


  getMyAddress(tx: Transaction) {

    if (tx.addressOut === this.address) {
      return tx.addressOut;
    }
    if (tx.addressIn === this.address) {
      return tx.addressIn;
    }

  }

  getOtherAddress(tx: Transaction) {
    if (tx.addressOut !== this.address) {
      return tx.addressOut;
    }
    if (tx.addressIn !== this.address) {
      return tx.addressIn;
    }
  }

  isIncoming(tx: Transaction) {
    return tx.addressIn === this.address;

  }


}
