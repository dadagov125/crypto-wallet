export class Transaction {


  id: number;

  created: Date;

  addressIn: string;

  addressOut: string;


  amount: number;

  fee: number;

}

