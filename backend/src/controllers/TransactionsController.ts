import {BodyParams, Controller, Get, PathParams, Post} from '@tsed/common';
import {TransactionService} from '../services/TransactionService';

@Controller('/transactions')
export class TransactionsController {

  constructor(private transactionService: TransactionService) {


  }

  @Post('/')
  getAllTransactions(@BodyParams() addresses: string[]) {
    return this.transactionService.getAllTransactions(addresses);

  }

  @Get('/:address')
  getTransactions(@PathParams('address') address: string) {
    return this.transactionService.getTransactions(address);

  }


}
