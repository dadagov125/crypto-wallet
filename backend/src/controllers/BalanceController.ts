import {Controller, Get, PathParams} from '@tsed/common';
import {TransactionService} from '../services/TransactionService';

@Controller('/balance')
export class BalanceController {

  constructor(private transactionService: TransactionService) {


  }

  @Get('/:address')
  async getBalance(@PathParams('address') address: string) {
    return {address, balance: await this.transactionService.getBalance(address)};
  }
}
