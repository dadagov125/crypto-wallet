import {Injectable} from '@tsed/di';
import * as bitcoin from 'bitcoinjs-lib';
import {Transaction} from '../entity/Transaction';
import {getRepository} from 'typeorm';
import {randomInt} from 'crypto';
import * as bip39 from 'bip39';

@Injectable()
export class TransactionService {

  async getTransactions(address: string): Promise<Transaction[]> {

    const repository = getRepository(Transaction);

    let transactions = await repository.createQueryBuilder('tx')
        .where('tx.addressIn = :address OR tx.addressOut = :address', {address}).getMany();
    if (transactions.length === 0) {
      transactions = await this.generateRandomTx(address);
    }

    return transactions;

  }


  async getAllTransactions(addresses: string[]) {

    if (!addresses || addresses.length === 0) {
      return [];
    }
    const repository = getRepository(Transaction);

    const query = repository.createQueryBuilder('tx')
        .where('tx.addressIn IN (:...addresses) OR tx.addressOut IN (:...addresses)', {addresses});


    const transactions = await query.getMany();

    const filtered = addresses.filter(a => !transactions.map(tx => tx.addressOut).includes(a) && !transactions.map(tx => tx.addressIn).includes(a));

    for (const a of filtered) {
      const txs: Transaction[] = await this.generateRandomTx(a);
      transactions.push(...txs);
    }

    return transactions;

  }


  async getBalance(address: string) {
    const txs = await this.getTransactions(address);
    if (!txs || txs.length === 0) {
      return 0;
    }
    const sumIn = txs.filter(tx => tx.addressIn === address).map(tx => tx.amount).reduce((prev, curr) => prev + curr, 0);
    const sumOut = txs.filter(tx => tx.addressOut === address).map(tx => tx.amount).reduce((prev, curr) => prev + curr, 0);
    const res = sumIn - sumOut;

    return res.toFixed(6);
  }


  private async generateRandomTx(address: string) {

    const randomCount = randomInt(1, 10);

    const txs = Array.from(new Array(randomCount), (x, i) => i + 1).map((value, index) => {
      const bip32Interface = bitcoin.bip32.fromSeed(bip39.mnemonicToSeedSync(bip39.generateMnemonic()), bitcoin.networks.testnet);
      const otherAddress = bitcoin.payments.p2pkh({pubkey: bip32Interface.publicKey}).address;
      if (!otherAddress) {
        throw new Error();
      }
      const randomType = randomInt(100);
      const tx = new Transaction();

      if (randomType % 2 === 0 || index === 0) {
        tx.addressIn = address;
        tx.addressOut = otherAddress;
      } else {
        tx.addressIn = otherAddress;
        tx.addressOut = address;
      }
      tx.created = new Date();
      tx.amount = Number((Math.random() * (2 - 0.01) + 0.01).toFixed(6));
      tx.fee = Number(((tx.amount / 100) * 0.1).toFixed(6));
      return tx;
    });

    await getRepository(Transaction)
        .createQueryBuilder()
        .insert()
        .into(Transaction)
        .values(
            txs
        )
        .execute();

    const repository = getRepository(Transaction);
    return await repository.createQueryBuilder('tx')
        .where('tx.addressIn = :address OR tx.addressOut = :address', {address}).getMany();

  }


}
