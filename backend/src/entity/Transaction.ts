import {Column, Entity, PrimaryGeneratedColumn} from 'typeorm';

@Entity()
export class Transaction {

  @PrimaryGeneratedColumn()
  id: number;
  @Column({type: 'timestamp'})
  created: Date;
  // @Column()
  // type: TransactionType;
  @Column()
  addressIn: string;

  @Column()
  addressOut: string;

  @Column({type: 'float8'})
  amount: number;

  @Column({type: 'float8'})
  fee: number;

}

export enum TransactionType {
  INCOMING = 'INCOMING',
  OUTGOING = 'OUTGOING'
}
